#!/bin/sh
SERVICEID="" # [0-9]{6},[0-9]{6}
REFERER="" # https://www.etermin.net/<name>
COOKIE="eTerminSessionId=" # 'eTerminSessionId=[a-z0-9]{23}'
USERAGENT="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0"

curl -s "https://www.etermin.net/api/timeslots?date=2021-"{01..12}"-"{01..31}"&serviceid=$SERVICEID&capacity=1&caching=false&duration=0&cluster=true&slottype=0&fillcalendarstrategy=0&showavcap=false&appfuture=10&appdeadline=2&msdcm=0&appdeadlinewm=0&tz=W.%20Europe%20Standard%20Time&tzaccount=W.%20Europe%20Standard%20Time&calendarid=" \
--referer "https://www.etermin.net/$REFERER" \
-b '$COOKIE' \
-H "webid: $REFERER" \
-H "Accept: application/json, text/plain" \
-H "Accept-Encoding: gzip, deflate, br" \
-H "Accept-Language en-US,en;q=0.5"  \
-H "Content-Type: application/json" \
-H "User-Agent: $USERAGENT"